package net.vsame.url2sql.url;

import javax.servlet.ServletContext;

import net.vsame.url2sql.url.impl.UrlMapping;
import net.vsame.url2sql.url.impl.UrlMapping.Chain;

public interface Interceptor {
	
	public void init(UrlMapping urlMapping, ServletContext servletContext);
	
	public void invoke(Chain chain);
	
	public void destroy();

}
