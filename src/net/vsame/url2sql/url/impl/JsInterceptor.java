package net.vsame.url2sql.url.impl;

import javax.servlet.ServletContext;

import net.vsame.url2sql.url.Interceptor;
import net.vsame.url2sql.url.impl.UrlMapping.Chain;

/**
 * 拦截器配置
 * @author <a href="mailto:gaollg@sina.com">Gaollg</a>
 *
 */
public class JsInterceptor implements Interceptor{

	private String regex;//url
	private String js;//jso
	
	public String getRegex() {
		return regex;
	}
	public void setRegex(String regex) {
		this.regex = regex;
	}
	public String getJs() {
		return js;
	}
	public void setJs(String js) {
		this.js = js;
	}
	
	@Override
	public void init(UrlMapping urlMapping, ServletContext servletContext) {
	}
	@Override
	public void invoke(Chain chain) {
	}
	@Override
	public void destroy() {
		
	}
	
}
