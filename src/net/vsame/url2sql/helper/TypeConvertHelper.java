package net.vsame.url2sql.helper;


public class TypeConvertHelper {
	
	@SuppressWarnings("unchecked")
	public static <T> T parseType(Class<T> clazz, Object o) {
		if(o == null){
			return null;
		}
		if(o.getClass() == clazz){
			return (T) o;
		}
		if(!clazz.isArray()){
			return parseType(clazz, o+"");
		}
		return null;
	}
	
	public static <T> T parseType(Class<T> clazz, Object o, T defVal) {
		T retVal = parseType(clazz, o);
		if(retVal == null) {
			retVal = defVal;
		}
		return retVal;
	}
	
	@SuppressWarnings("unchecked")
	private static <T> T parseType(Class<T> clazz, String o2){
		if(o2 == null){
			return null;
		}
		Object o = null;
		if(clazz==short.class || clazz==Short.class){
			o = Short.parseShort(o2);
		}	
		else if(clazz==int.class || clazz==Integer.class){
			o = Integer.parseInt(o2);
		}
		else if(clazz==long.class || clazz==Long.class){
			o = Long.parseLong(o2);
		}
		else if(clazz==double.class || clazz==Double.class){
			o = Double.parseDouble(o2);
		}
		else if(clazz==float.class || clazz==Float.class){
			o = Float.parseFloat(o2);
		}
		else if(clazz==boolean.class || clazz==Boolean.class){
			o = Boolean.parseBoolean(o2);
		}
		else if(clazz == String.class){
			o = o2;
		}
		// byte char
		if(o == null){
			return null;
		}
		return (T) o;
	}
	
}
