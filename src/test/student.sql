/*
Navicat MySQL Data Transfer

Source Server         : 10.0.2.2_3306
Source Server Version : 50534
Source Host           : 10.0.2.2:3306
Source Database       : url2sql

Target Server Type    : MYSQL
Target Server Version : 50534
File Encoding         : 65001

Date: 2014-02-28 16:20:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `student`
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('1', 'Huhu', '0');
INSERT INTO `student` VALUES ('2', 'gaollg', '25');
INSERT INTO `student` VALUES ('3', '高雷', '25');
INSERT INTO `student` VALUES ('4', '你好', '33');
INSERT INTO `student` VALUES ('6', 'NiHaoJS', '22');
