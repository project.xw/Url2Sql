package test;

import javax.servlet.ServletContext;

import net.vsame.url2sql.url.Interceptor;
import net.vsame.url2sql.url.impl.UrlMapping;
import net.vsame.url2sql.url.impl.UrlMapping.Chain;

public class TestInterceptor implements Interceptor{

	@Override
	public void init(UrlMapping urlMapping, ServletContext servletContext) {
		System.out.println("==========================init==================");
	}

	@Override
	public void invoke(Chain chain) {
		System.out.println("-----------------------invoke start------------------");
		long begin = System.currentTimeMillis();
		try {
			chain.next();
		} finally {
			System.out.println((System.currentTimeMillis()-begin) + "ms");
			System.out.println("-----------------------invoke end------------------");
		}
		
	}

	@Override
	public void destroy() {
		
	}

	
}
