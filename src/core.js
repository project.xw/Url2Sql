var version = java.lang.System.getProperty("java.version");
if (version.startsWith("1.8.0")) {
    load("nashorn:mozilla_compat.js");
}
importPackage(java.util);

//query,page,queryOne,execute,executeGeneratedKey,alltables
var SQL = {};
(function(){
	var ms = ['query', 'queryOne', 'execute', 'executeGeneratedKey'];
	var toArray = function(args, beginIndex){
		var list = new ArrayList();
		for(var i = beginIndex; i<args.length; i++){
			var a = args[i];
			if(a instanceof Array){
				var tempList = new ArrayList();
				for(var temp=0; temp<a.length; temp++){
					tempList.add(a[temp]);
				}
				list.add(tempList.toArray());
			}else {
				list.add(a);
			}
		}
		return list.toArray();
	};
	for(var i=0; i<ms.length; i++){
		(function(m){
			SQL[m] = function(sql){
				var array = toArray(arguments, 1);
				return net.vsame.url2sql.helper.SqlHelper[m](sql, array);
			};
		})(ms[i]);
	}
	SQL.page = function(currentpage, pagesize, column, where, other){
		if(typeof currentpage == 'number'){
			var array = toArray(arguments, 4);
			return net.vsame.url2sql.helper.SqlHelper.page(currentpage, pagesize, column, where, array);
		}
		var array = toArray(arguments, 2);
		return net.vsame.url2sql.helper.SqlHelper.page(currentpage, pagesize, array);
		
	};
	SQL.page = function(currentpage, pagesize, column, where, other){
		if(typeof currentpage == 'number'){
			var array = toArray(arguments, 4);
			return net.vsame.url2sql.helper.SqlHelper.page(currentpage, pagesize, column, where, array);
		}
		var array = toArray(arguments, 2);
		return net.vsame.url2sql.helper.SqlHelper.page(currentpage, pagesize, array);
		
	};
	SQL.alltables = function(){
		return net.vsame.url2sql.helper.SqlHelper.alltables();
	};
})();


function execute(js, context, errors){
	//校验失败需返加code值和msg
	if(js){
		//context就不写了, 用闭包吧, 其它的用闭包Eclipse有警告, 太烦了
		var param = "request, response, session, urlConfig, SqlHelper";
		var real = "context.getRequest(), "
			+ "context.getResponse(), "
			+ "context.getServletSession(), "
			+ "context.getUrlConfig(), "
			+ "SQL";
		try{
			eval('((function(' + param + '){' + js + '})(' + real + '))');//.call
		}catch(e){
			var m = e;
			if(e.message){
				m = e.message;
			}
			errors.add(m+"");
		}
	}
};
